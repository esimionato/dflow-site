---
title: 'En Estudios Juridicos'
icon: '/abogados.svg'
iconColor: '/abogados_color.svg'
heroPic: '/abogados_land_01.jpg'
description: 'Mantiene al alcance los expedientes y toda la documentacion de tu estudio. Logrando que la gestion y control de tus casos y clientes se vuelva sencillo.'
heroDescription: 'Mantiene al alcance todos los documentos y expedientes de tu estudio. Logrando que la gestion y control de tus casos y clientes se vuelva sencillo.'
overview: 'Mantiene al alcance todos los documentos y expedientes de tu estudio. Logrando que la gestion y control de tus casos y clientes se vuelva sencillo.'
order: 3
themeColor: '#129F00'
themeLight: 'rgba(18,159,0, 0.1)'
codeGithubUrl: 'https://github.com/chromaui/learnstorybook-code'
heroAnimationName: 'float'
toc: ['tips-implementacion', 'construir-flujos', 'gestion-administracion']
coverImagePath: '/guide-cover/visual-testing.svg'
thumbImagePath: '/guide-thumb/visual-testing.svg'
contributorCount: '+2'
authors:
  [
    {
      src: 'https://avatars2.githubusercontent.com/u/263385',
      name: 'Dominic Nguyen',
      detail: 'Storybook design',
    },
    {
      src: 'https://avatars2.githubusercontent.com/u/132554',
      name: 'Tom Coleman',
      detail: 'Storybook core',
    },
  ]
contributors:
  [
    {
      src: 'https://avatars2.githubusercontent.com/u/239215',
      name: 'Fernando Carrettoni',
      detail: 'Design Systems at Auth0',
    },
  ]
---

<h2>Pensado para simplificar tus actividades</h2>

![CommentList](/visual-testing-handbook/commentlist-presentation-data.jpg)

Por que DFLOW ?

- 🏗 Gestion de expedientes y documental
- 🎨 Control y gestion economica
- 📕 Agenda de vencimiento y avisos

Miles de profesionales aman utilizar DFLOW.
