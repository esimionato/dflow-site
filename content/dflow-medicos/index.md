---
title: 'Para profesionales de la salud'
alias: 'Organizaciones Medicas'
icon: '/medicos.svg'
iconColor: '/medicos_color.svg'
heroPic: '/medicos_land.jpg'
description: 'Integra los procesos desde la gestion de turnos hasta la centralizacion de historias clinicas y control de finanzas.'

heroDescription: "Sistema integral para la gestion de clinicas y profesionales de la salud.  Integra los procesos desde la gestion de turnos hasta la centralizacion de historias clinicas y control de finanzas.
"

overview: " Brindamos a las organizaciones medicas una forma innovadora para optimizar las consultas a historias clinicas de sus pacientes.
Dflow se integra a sus procesos desde la identificacion hasta la disposicion, brindando soluciones a problemas de gestion, seguimiento y control."
order: 2
themeColor: '#0079FF'
themeLight: 'rgba(0,121,255, 0.1)'
codeGithubUrl: 'https://github.com/chromaui/learnstorybook-design-system'
heroAnimationName: 'float'
toc:
  [
    'tips-implementacion',
    'construir-flujos',
    'gestion-administracion',
  ]
coverImagePath: '/guide-cover/design-system.svg'
thumbImagePath: '/guide-thumb/design-system.svg'
contributorCount: '0'
authors:
  [
    {
      src: 'https://avatars2.githubusercontent.com/u/263385',
      name: 'Dominic Nguyen',
      detail: 'Storybook design',
    },
    {
      src: 'https://avatars2.githubusercontent.com/u/132554',
      name: 'Tom Coleman',
      detail: 'Storybook core',
    },
  ]
contributors: [
    {
      src: 'https://avatars2.githubusercontent.com/u/239215',
      name: 'Fernando Carrettoni',
      detail: 'Design Systems at Auth0',
    },
]
twitterShareText: "I’m learning about building design systems! They're great for scaling frontend code on large teams."
---

<h2>Pensado para simplificar tus actividades</h2>

![Design System example](/design-systems-for-developers/design-system-overview.jpg)

Por que DFLOW ?

- 🏗 Optimiza el proceso de atencion medica
- 🎨 Organiza la genstion de turnos y eveita el ausentismo.
- 📕 Registra datos confiables en la Historia Clinica Digital.

Miles de profesionales aman utilizar DFLOW.
