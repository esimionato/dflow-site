---
title: 'Para las PYMES'
alias: 'PEQUENA y MEDIANA EMPRESA'
icon: '/pymes.svg'
iconColor: '/pymes_color.svg'
heroPic: '/pymes_land.jpg'
description: 'Tus procesos con flujos sencillos facilitando el control y seguimiento de tu negocio.'
heroDescription: 'Con dFlow vas a poder mejorar los procesos mediate flujos sencillos. Estos flujos van a ayudarte en el control y seguimiento sus tareas diarias, asi te podes enfocar rapidamente en el negocio.'
overview: 'Flow permite mejorar los procesos mediate la implementacion de flujos sencillos. Estos flujos permiten a la empresa tomar el control y seguimiento sus tareas diarias, facilitando asi el control y seguimiento del negocio en todos los aspectos.'
order: 1
themeColor: '#6F2CAC'
themeLight: 'rgba(111,44,172, 0.1)'
codeGithubUrl: 'https://github.com/chromaui/learnstorybook-code'
heroAnimationName: 'float'
toc: ['tips-implementacion', 'construir-flujos', 'gestion-administracion', 'gestion-mi-dflow']
coverImagePath: '/guide-cover/intro.svg'
thumbImagePath: '/guide-thumb/intro.svg'
contributorCount: '+10'
authors:
  [
    {
      src: 'https://avatars2.githubusercontent.com/u/263385',
      name: 'Dominic Nguyen',
      detail: 'Storybook design',
    },
    {
      src: 'https://avatars2.githubusercontent.com/u/132554',
      name: 'Tom Coleman',
      detail: 'Storybook core',
    },
  ]
contributors:
  [
    {
      src: 'https://avatars2.githubusercontent.com/u/239215',
      name: 'Fernando Carrettoni',
      detail: 'Design Systems at Auth0',
    },
  ]
twitterShareText: ''
---

<h2>Funcinalidades que hacen la diferencia</h2>

![Design System example](/design-systems-for-developers/design-system-overview.jpg)

Por que DFLOW ?

- 🏗 Optimiza el proceso de atencion
- 🎨 Organiza la genstion de turnos y eveita el ausentismo.
- 📕 Registra datos confiables en la Historia.

Miles de profesionales aman utilizar DFLOW.
