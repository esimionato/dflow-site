const isDeployPreview = process.env.CONTEXT === 'deploy-preview';
const permalink = isDeployPreview ? process.env.DEPLOY_PRIME_URL : 'https://learnstorybook.com';

module.exports = {
  siteMetadata: {
    title: `Dflow Starter`,
  "description": "A simple starter of dflow site with love <3 m|^_^|m. ",
    author: `@ernesto`,
    permalink: permalink,
    siteUrl: permalink,
    logo: '/icon-flow.svg',
    prelogo: 'FLOW',
    githubUrl: 'https://github.com/dflow/dflow',
    contributeUrl: 'https://github.com/chromaui/learnstorybook.com/#contribute',
    logo: "/icon-dflow.svg",
    pages: {
	home: {
	   stats: [
		{ name: "Cliente", count: 15, message: "Acompanandolos con soluciones digitales en sus proyectos mas estrategicos.", more: 0   },
		{ name: "Flujos", count: 250, message: "Adaptandonos al mercado digital, desde la banca digital hasta plataformas educativas." , more: 0  },	
		{ name: "Tickets creados", count: 1000, message: "Un Millar de procesos se completaron, logrando eficacia y eficiencia en los negocios.", more: 1   }
	   ]
	}
    }

  },
  plugins: [
    {
      resolve: 'gatsby-plugin-layout',
      options: {
        component: require.resolve(`./src/components/composite/AppLayout.js`),
      },
    },
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
      path: `${__dirname}/content/`,
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-sitemap`,
  ],
};
