const isDeployPreview = process.env.CONTEXT === 'preview';
const permalink = isDeployPreview ? process.env.DEPLOY_PRIME_URL : 'https://www.dflow.com.ar';

module.exports = {
  siteMetadata: {
    title: `Dflow Starter`,
  "description": "A simple starter of dflow site with love <3 m|^_^|m. ",
    author: `@ernesto`,
    permalink: permalink,
    siteUrl: permalink,
    logo: '/icon-flow.svg',
    prelogo: 'FLOW',
    company: {
	name: 'DFLOW'
    }
  },
  plugins: [
     {
	  resolve: `gatsby-plugin-layout`,
	  options: {
	    component: require.resolve('./src/v0/components/composite/AppLayout.js'),
	  },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
