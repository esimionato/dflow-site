import React from 'react';
import styled from "styled-components";

const FloatBox = styled.div`
  position: fixed;
  top: 10px;
  right: 10px;
  max-height: 60vh;
  max-width: 60vw;
  background: rgba(0, 0, 0, 0.75);
  color: lime; 
  overflow-y: auto;
  
  & > * {
    font-weight: 700;
    font-family: 'Inconsolata', monospace;
 }

`;
/*background: rgba(255, 71, 133, 0.1);*/

export const JsonPre = (props) => (
<FloatBox>
    <pre>{JSON.stringify(props.src,
	    (key, value) => {
		if(value === undefined) {
		  return "";
		}
		return value;
	    }, 2)}</pre>
</FloatBox>
);


export default { JsonPre };
