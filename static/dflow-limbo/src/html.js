import React from 'react';
import PropTypes from 'prop-types';
import { global } from '@storybook/design-system';

export default function HTML(props) {
 return (
<html {...props.htmlAtributes}>

<head>

      <meta charSet="utf-8" />

      <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no" />

      {props.headComponents}

     <link href={global.fontUrl} rel="stylesheet" />

     <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap" rel="stylesheet" />
     <link href="https://fonts.googleapis.com/css?family=Inconsolata:400,500,700,900&display=swap" rel="stylesheet" />

</head>

<body {...props.bodyAtributes}>

  {props.preBodyComponents}

  <noscript key="noscript" id="gatsby-noscript">
    Must be Javascript enabled.
  </noscript>
 
  <div key="body" id="___gatsby" dangerouslySetInnerHTML={{ __html: props.body }} />

  {props.postBodyComponents}

</body>

</html>

);
}
