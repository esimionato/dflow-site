import React from 'react';
import PropTypes from 'prop-types';
import { graphql, StaticQuery } from 'gatsby';
import { global } from '@storybook/design-system';

import { JsonPre } from '../../../debug';

import Helmet from 'react-helmet';
import Header from './Header';
import Footer from './Footer';

const { GlobalStyle } = global;

const query = graphql`
  query TemplateWrapper {
    site {
      siteMetadata {
        title
	description
	permalink
	logo
	prelogo
	company {
	  name
	}
      }
    }
  }
`;


export const LayoutTemplate = (children, data, ...props) =>  {
return (<>

<Header />
<Footer />

</>);
}
  

const TemplateWrapper = (
  { location: { pathname }, children, ...props } ) => {

return(

<StaticQuery query={query} render={(data) => (
   <>

   <Header /> 
	<JsonPre src={data}/>
   	{children}
   <Footer />

   </>)}

/>);

}

TemplateWrapper.propTypes = {
  children: PropTypes.any.isRequired,
  location: PropTypes.shape({
  pathname: PropTypes.string.isRequired,
 }).isRequired,
};


export default TemplateWrapper;
