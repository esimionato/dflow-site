import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import styled from 'styled-components';
import { Link } from 'gatsby';
import {
	styles,
	Icon,
	Link as AnimateLink,
	Subheading,
	TooltipLinkList,
	WithToolTip
} from '@storybook/design-system';
import Logo from '../basics/Logo';

export const Header = () => {

return(
<>
  HEADER
  <Logo/>
</>
);

}

export default Header;
